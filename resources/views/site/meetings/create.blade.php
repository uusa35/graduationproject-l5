@extends('layouts._tow_col')

@section('styles')
    @parent
    <link rel="stylesheet" href="../css/bootstrap-timepicker.min.css"/>
    <link rel="stylesheet" href="http://davidstutz.github.io/bootstrap-multiselect/dist/css/bootstrap-multiselect.css"/>
@stop

@section('sub-content')
    @parent
    <h1>Create A Meeting</h1>


        @if($errors->has())
            <div class="alert alert-danger" role="alert">
            @foreach($errors->all() as $error)
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                {!! $error !!}</br>
            @endforeach
            </div>
        @endif



    {!! Form::open(['action'=>'MeetingController@store','Method'=>'post']) !!}
        <div class="form-group col-md-6">
            {!! Form::label('subject') !!}
                {!! Form::text('subject',null,['class'=>'form-control','placeholder'=>'Add a Subject']) !!}
        </div>
        <div class="col-md-6 form-group">
            {!! Form::label('date','Meeting Date : ') !!}
            {!! Form::date('date',Carbon\Carbon::now(), ['class'=>'form-control']) !!}
        </div>
    <div class="col-md-6 form-group">
        {!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Description']) !!}
    </div>
        <div class="form-group col-md-6">
            {!! Form::label('Attendees :') !!}
            {!! Form::select('attendees',$allAttendees,null,['multiple'=>'multiple','name'=>'attendees[]','class'=>'form-control','placeholder'=>'Add a Subject','id'=>'attendee']) !!}
        </div>
        <div class="col-md-6 form-group">
            {!! Form::label('timepicker1', 'Meeting Time :') !!}
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1">@</span>
                   {{-- <input id="timepicker1" type="text" name="time" class="input-small form-control">--}}
                    {!! Form::text('time',null,['class'=>'input-small form-control','id'=>'timepicker1'])!!}
                </div>
        </div>



        <div class="col-md-12 form-group">
            {!! Form::hidden('user_id', Auth::user()->id) !!}
            {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}
@stop

@section('scripts')
    @parent
    <script type="javascript" src="http://davidstutz.github.io/bootstrap-multiselect/dist/css/bootstrap-multiselect.css"></script>
    <script>
        $('#timepicker1').timepicker();

    </script>
@stop
