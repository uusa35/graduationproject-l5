@extends('layouts._tow_col')


@section('sub-content')
    @parent
    <div class="row">
        <div class="col-lg-12 col-md-12"><h1>Invitations</h1></div>
        @if(count($allMeetings) > 0)
            @foreach($allMeetings as $meeting)
                <div class="col-lg-12 col-md-12" style="">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4>{!! link_to_action('MeetingController@getShowMeeting',$meeting->subject,$meeting->id,null) !!}</h4>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-4 col-md-4">
                                    <a href="#">
                                        <img class="img-responsive" alt="Bootstrap template" src="http://loremicon.com/i/flat/64/computer">
                                    </a>
                                </div>
                                <div class="col-lg-8 col-md-8">
                                    <p class="text-justify">
                                        {!! str_limit($meeting->description, $limit = 80, link_to_action('MeetingController@getShowMeeting','..more',$meeting->id,null)) !!}
                                    </p>
                                    <div class="text-left">
                                        <a href="{!! route('meeting.create') !!}"><span class="glyphicon glyphicon-time"></span> {!! date('d-m-Y', strtotime(Carbon\Carbon::now()->now()->subDays($meeting->id*2))) !!}  </a>
                                        <a href="#"><span class="glyphicon glyphicon-hourglass"></span> {!! date('H:i', strtotime(Carbon\Carbon::now()->now()->subHours($meeting->id*3))) !!}</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

            @endforeach
        @else
            <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only"></span>
                No Meetings Invitations !!!!</br>
            </div>

        @endif
    </div>
@stop




