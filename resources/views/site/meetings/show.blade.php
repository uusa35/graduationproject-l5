@extends('layouts._tow_col')


@section('sub-content')
    @parent
    <div class="row">

            {{--TOW MEETINGS WITHIN ONE ROW --}}
            <div class="col-lg-12 col-md-12" style="">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>{!! $meeting->subject !!}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-4 col-md-4">
                            <a href="#">
                                <img class="img-responsive" alt="Bootstrap template" src="http://loremicon.com/i/flat/128/computer">
                            </a>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <p class="text-justify">
                                {!! $meeting->description !!}
                            </p>
                            <div class="text-left">
                                <span class="glyphicon glyphicon-time"></span> {!! date('d-m-Y', strtotime(Carbon\Carbon::now()->now()->subDays($meeting->id*2))) !!}
                                <span class="glyphicon glyphicon-hourglass"></span> {!! date('H:i', strtotime(Carbon\Carbon::now()->now()->subHours($meeting->id*3))) !!}
                            </div>
                        </div>

                    </div>
                </div>

            </div>


    </div>

@stop




