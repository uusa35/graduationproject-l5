@extends('master')

    @section('styles')
        @include('partials.styles')
    @stop

    @section('content')
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    {{--@if(Auth::check())
                        <div class="col-lg-1">
                            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle">
                                <span class="glyphicons glyphicon glyphicon-th"></span></a>
                        </div>
                    @endif--}}
                    <div class="col-lg-12">
                        @if(Session::get('message'))
                            <div class="alert alert-success" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Success:</span>
                                    {!! Session::get('message') !!}
                            </div>
                        @endif
                        @yield('sub-content')
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    @stop

    @section('main')
        @yield('content')
    @stop

    @section('scripts')
        @include('partials.scripts')
    @stop