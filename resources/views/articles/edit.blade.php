@extends('app')
@section('content')
    <div class="row">
    <h1>edit article</h1>
    {!! Form::model($article,['action'=>'ArticleController@update','method'=>'PATCH']) !!}
        @include('articles.partials._form')
    {!! Form::close() !!}
    </div>
@endsection