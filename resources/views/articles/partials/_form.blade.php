<div class="form-group {!! $errors->has('title') ? 'has-error' : '' !!}">
    {!! Form::label('title') !!}
    {!! Form::text('title',null,['class'=>'form-control']) !!}
    {!! $errors->first('title','<span class="help-block">:message</span>')!!}
</div>
<div class="form-group {!! $errors->has('slug') ? 'has-error' : '' !!}">
    {!! Form::text('slug',null,['class'=>'form-control']) !!}
    {!! $errors->first('slug','<span class="help-block">:message</span>')!!}
</div>
<div class="form-group {!! $errors->has('body') ? 'has-error' : '' !!}">
    {!! Form::textarea('body',null,['class'=>'form-control']) !!}
    {!! $errors->first('body','<span class="help-block">:message</span>')!!}
</div>
<div class="form-group">
    {!! Form::submit('submit',['class'=>'btn btn-primary']) !!}
</div>
</div>