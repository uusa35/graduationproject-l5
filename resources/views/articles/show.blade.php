@extends('app')
@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">{!! $article->title !!}</div><a class="btn btn-default pull-right" href="{{ action('ArticleController@edit',$article->slug) }}">edit</a>
            <div class="panel-body">
                <a href="{{ URL::action('ArticleController@show', $article->slug) }}">{!! $article->slug !!}</a></br>
                {!! $article->body !!}
            </div>
        </div>

@endsection
