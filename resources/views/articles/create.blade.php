@extends('app')
@section('content')
    <div class="row">
    <h1>Add article</h1>
    {!! Form::open(['action'=>'ArticleController@store','method'=>'Post']) !!}
        @include('articles.partials._form')
    {!! Form::close() !!}
    </div>
@endsection