<!-- jQuery -->
<script src="../js/app.js"></script>


<!-- Menu Toggle Script -->
<script>

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
