<!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">

                OSMP

            </li>
            <li>
                <a href="{!! action('MeetingController@index') !!}"><i class="glyphicon glyphicon-home"></i>  | Home</a>
            </li>
            <li>
                <a href="{!! action('MeetingController@create') !!}"><i class="glyphicon glyphicon-globe"></i> | Create a Meeting</a>
            </li>
            <li>
                <a href="{!! action('MeetingController@show',['id'=>Auth::id()]) !!}"><i class="glyphicon glyphicon-inbox"></i> | My Inbox</a>
            </li>
            <li>
                <a href="{!! route('auth.logout') !!}"><i class="glyphicon glyphicon-remove-circle" ></i> | Sign out</a>
            </li>

        </ul>
    </div>
    <!-- /#sidebar-wrapper -->
