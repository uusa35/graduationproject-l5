<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>University</title>
    @yield('styles')
</head>

<body>
    @include('partials.header')
<div id="wrapper">
    @if(Auth::user())
        @include('partials.sidebar')
    @endif
    <div class="row">
        @yield('main')
    </div>

</div>
<!-- /#wrapper -->

@section('scripts')
    @include('partials.scripts')
@stop

@yield('scripts')
</body>

</html>
