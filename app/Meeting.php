<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model {

	//
    protected $table ='meetings';
    protected $fillable = [
        'subject',
        'description',
        'date',
        'time',
        'user_id'
    ];

    // a meeting belongs to one user
    public function users () {
        return $this->belongsToMany('User');
    }

}
