<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateMeetingRequest;
use App\Acme\Meetings\MeetingRepository;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class MeetingController extends Controller {


	public $meeting;
	public $attendees;
	public function __construct(MeetingRepository $model, User $user) {
		$this->meeting = $model;
		$this->user = $user;
	}

    /*
		Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//

		$allMeetings = $this->meeting->model->orderBy('created_at','desc')->paginate(6);
		return View('site.meetings.index', compact('allMeetings'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function create()
	{
		//
		$allAttendees = DB::table('users')->lists('name','id');
		return View('site.meetings.create',compact('allAttendees'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateMeetingRequest $request)
	{
		//
		$createdMeeting = $this->meeting->model->create($request->all());
		if($createdMeeting) {
			foreach ($request->get('attendees') as $attendee) {
				DB::table('meeting_user')->insert(['user_id'=> $attendee, 'meeting_id' => $createdMeeting->id]);
			}
		return redirect(route('home'))->with('message',' Meeting created successfully .. thank you');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//

		// all meetings that is related to one user
		$allMeetings =  $this->user->find($id)->meetings;
		//$allMeetings =  $this->meeting->model->where('user_id','=',$id)->OrderBy('created_at','desc')->paginate(6);
		return View('site.meetings.inbox',compact('allMeetings'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	function getShowMeeting($id){

		$meeting =  $this->meeting->model->find($id);
		return View('site.meetings.show',compact('meeting'));
	}

}
