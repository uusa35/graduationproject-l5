<?php namespace App\Http\Controllers;

use App\Acme\Articles\ArticleRepository;
use App\Http\Requests\CreateArticleRequest;



class ArticleController extends Controller {

	public $article;

	public function __construct(ArticleRepository $article) {

		$this->article = $article;


	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		//return view('layouts.index');
		return View('partials.home')->with('test','testing value');
		$allArticles = $this->article->getAll();
		return view('articles.index',compact('allArticles'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View('articles.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateArticleRequest $request)
	{
		//
		$this->article->model->create($request->all());
		return redirect()->route('articles.index');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		//
		// if the function is not directly defined we have to access the model variable that were instanited.
		$article = $this->article->model->where('slug','=',$slug)->first();
		return view('articles.show',compact('article'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($slug)
	{
		//

		/*the model is typed here because this funciton is not in the abstract which is "where" therefore you have to access to the $model variable which
		represents the Elquant model itself other wise you have to define a function like getAll within the abstract*/
		$article = $this->article->model->where('slug','=',$slug)->first();
		return view('articles.edit',compact('article'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CreateArticleRequest $request)
	{
		//

		$article = $this->article->model->where('slug','=',$request->input('slug'))->first();

		//$article->title = $request->input('title');

		$article->fill($request->all())->save();
		//$article->save();

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
