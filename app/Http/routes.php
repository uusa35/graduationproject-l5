<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*Route::get('/design/pattern', function () {
	$redDuck = new App\Acme\Duck\RedHeadDuck();
	$RubberDuck = new App\Acme\Duck\RubberDuck();

	// it can quack
	echo (($redDuck->performFly()));
	echo '</br>';
	// it can not fly
	echo ($redDuck->performQuack());
	echo '</br>';

	// it can not fly
	echo (($RubberDuck->performFly()));
	echo '</br>';
	// it can quack
	echo ($RubberDuck->performQuack());
});*/




Route::group(['middleware' => 'auth'], function() {

	Route::get('/{home?}', ['as'=>'home', 'uses'=>'MeetingController@index']);
	Route::get('auth/logout',['as'=>'auth.logout','uses'=>'Auth\AuthController@getLogout']);
	Route::resource('meeting','MeetingController');
	Route::get('/meeting/show/{id}','MeetingController@getShowMeeting');


});

Route::group(['middleware'=>'guest'], function () {

	Route::get('/auth/login','Auth\AuthController@getLogin');
	Route::post('/auth/login','Auth\AuthController@postLogin');
	Route::get('auth/register',['as'=>'auth.register','uses'=>'Auth\AuthController@getRegister']);
	Route::post('auth/register','Auth\AuthController@postRegister');

});






//Route::get('/login',['as'=>'login.auth','uses'=>'AuthController']);

/*Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);*/
