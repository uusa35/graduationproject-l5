<?php namespace App\Acme\Articles;

use App\Acme\AbstractRrepository;
use App\Article;
use App\Acme\InterfaceRepository;

/**
 * Created by PhpStorm.
 * User: usama
 * Date: 2/16/2015
 * Time: 1:43 PM
 */

class ArticleRepository extends AbstractRrepository implements InterfaceRepository {

    public $model;

    public function __construct (Article $model) {
        $this->model = $model;
    }



}