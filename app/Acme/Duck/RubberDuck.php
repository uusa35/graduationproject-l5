<?php namespace App\Acme\Duck;
/**
 * Created by PhpStorm.
 * User: usama_000
 * Date: 2/14/2015
 * Time: 4:11 PM
 */




class RubberDuck extends Duck {


    public function __construct() {

        $this->flyBehviour = new CanNotFly();
        $this->quackBehviour = new CanQuack();
    }

    public function RubberDuckFun() {
    var_dum('testing from RubberDuck Function');
    }

}

