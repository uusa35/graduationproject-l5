<?php namespace App\Acme\Duck;
/**
 * Created by PhpStorm.
 * User: usama_000
 * Date: 2/14/2015
 * Time: 4:07 PM
 */



abstract class Duck {

    public $flyBehviour;
    public $quackBehviour;

    // interfaces never been instanicated but this is to empose the injection to instaniate a class that has fly/quack method declared within
    public function setFlyBehviour (FlyBehviour $flyBehviour) {
        $this->flyBehviour = $flyBehviour;
    }

    public function setQuackBehviour (QuackBehviour $quackBehviour) {
        $this->quackBehviour = $quackBehviour;
    }

    public function display() {
        echo 'this is from the display method';
    }

    public function performFly () {
        return  get_called_class(). $this->flyBehviour->fly();
    }

    public function performQuack() {
       return get_called_class(). $this->quackBehviour->Quack();
    }

}