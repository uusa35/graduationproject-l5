<?php
/**
 * Created by PhpStorm.
 * User: usama_000
 * Date: 2/14/2015
 * Time: 4:42 PM
 */

namespace App\Acme\Duck;


class RedHeadDuck extends Duck {

// it can fly and it can quack
    public function __construct() {
        $this->setFlyBehviour(new CanFly());
        $this->setQuackBehviour(new CanQuack());
    }
}