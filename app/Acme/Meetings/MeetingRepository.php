<?php
namespace App\Acme\Meetings;
/**
 * Created by PhpStorm.
 * User: usamaahmed
 * Date: 3/30/15
 * Time: 12:39 PM
 */

use App\Meeting;
use App\User;
use Illuminate\Support\Facades\DB;

class MeetingRepository extends \App\Acme\AbstractRrepository implements \App\Acme\InterfaceRepository {


    public $model;
    public function __construct (Meeting $model) {
        $this->model = $model;
    }


}