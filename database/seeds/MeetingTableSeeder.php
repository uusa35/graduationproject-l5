<?php
/**
 * Created by PhpStorm.
 * User: usama_000
 * Date: 2/19/2015
 * Time: 5:57 PM
 */

use Illuminate\Database\Seeder as Seeder;
use App\Meeting;

class MeetingTableSeeder extends Seeder {


    public function run () {

      // DB::table('meetings')->truncate();
        $faker = Faker\Factory::create();
        for($i=0;$i<=20;$i++) {
           Meeting::create([
                'subject'           => $faker->sentence("1"),
                'description'       => $faker->paragraph("2"),
                'user_id'           => $faker->numberBetween(10,80),
                'date'              => $faker->date('Y-m-d','now'),
                'time'              => $faker->time('H:i:s','now')
            ]);
        }
        $this->command->info('User table seeded!');
    }

}