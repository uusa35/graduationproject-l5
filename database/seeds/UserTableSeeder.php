<?php
/**
 * Created by PhpStorm.
 * User: usama_000
 * Date: 2/19/2015
 * Time: 5:57 PM
 */
use Illuminate\Database\Seeder as Seeder;
use App\User;


class UserTableSeeder extends Seeder {


    public function run () {

//        DB::table('users')->truncate();
        $faker = Faker\Factory::create();
        for($i=0;$i<=5;$i++) {
           User::create([
                'name'     => $faker->name,
                'email'      => $faker->email,
               'password'   => $faker->text('1234'),
               'major'      => $faker->name,
            ]);
        }
        $this->command->info('User table seeded!');
    }

}