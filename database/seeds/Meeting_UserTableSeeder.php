<?php
/**
 * Created by PhpStorm.
 * User: usama_000
 * Date: 2/19/2015
 * Time: 5:57 PM
 */
use Illuminate\Database\Seeder as Seeder;
use App\Meeting;

class Meeting_UserableSeeder extends Seeder {


    public function run () {

        DB::table('users')->truncate();
        $faker = Faker\Factory::create();
        for($i=0;$i<=5;$i++) {
           DB::table('meetings')->create([
               'user_id'           => $faker->randomDigit([3,6]),
               'meeting_id'        => $faker->randomDigit([0,5]),
            ]);
        }
        $this->command->info('User table seeded!');
    }

}