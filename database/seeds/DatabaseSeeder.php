<?php

use Illuminate\Database\Seeder as Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->call('UserTableSeeder');
		$this->call('MeetingTableSeeder');
		//$this->call('Meeting_UserTable');
	}

}
